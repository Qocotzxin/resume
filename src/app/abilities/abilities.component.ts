import { Component, OnInit } from "@angular/core";
import { Chart } from "chart.js";
@Component({
  selector: "app-abilities",
  templateUrl: "./abilities.component.html",
  styleUrls: ["./abilities.component.css"]
})
export class AbilitiesComponent implements OnInit {
  titles: boolean = true;

  database: boolean;
  repository: boolean;
  os: boolean;
  ide: boolean;
  layout: boolean;
  frontend: boolean;
  backend: boolean;
  frontFramework: boolean;
  backFramework: boolean;
  layoutFramework: boolean;
  testing: boolean;
  design: boolean;
  learning: boolean;
  working: boolean;

  chart: Chart;

  constructor() {}

  ngOnInit() {}

  public generateChart(type: string) {
    this.titles = false;
    this.setVisibility(type);
    let data = this.setDataset(type);

    this.chart = new Chart("chart", {
      type: "horizontalBar",
      data: data,
      options: {
        responsive: false,
        scales: {
          xAxes: [
            {
              ticks: {
                min: 0,
                max: 100 // Edit the value according to what you need
              }
            }
          ],
          yAxes: [
            {
              stacked: true
            }
          ]
        }
      }
    });
  }

  back() {
    this.chart.destroy();
    this.titles = true;
    this.database = false;
    this.repository = false;
    this.os = false;
    this.ide = false;
    this.layout = false;
    this.frontend = false;
    this.backend = false;
    this.frontFramework = false;
    this.backFramework = false;
    this.layoutFramework = false;
    this.testing = false;
    this.design = false;
    this.learning = false;
    this.working = false;
  }

  setDataset(type: string) {
    let data;

    switch (type) {
      case "database":
        data = {
          datasets: [
            {
              label: "Bases de datos",
              data: [65, 30, 10],
              backgroundColor: ["#E48E00", "#50AA4B", "#336791"],
              hoverBorderColor: ["#000000", "#000000", "#000000"]
            }
          ],
          labels: ["MySQL", "MongoDB", "Otras (Postgres, MSSQL, Firebase)"]
        };
        break;
      case "repository":
        data = {
          datasets: [
            {
              label: "Repositorios/Versionado",
              data: [100],
              backgroundColor: ["#F15034"],
              hoverBorderColor: ["#000000"]
            }
          ],
          labels: ["git"]
        };
        break;
      case "os":
        data = {
          datasets: [
            {
              label: "Sistemas operativos",
              data: [60, 80],
              backgroundColor: ["#000000", "#00ADEF"],
              hoverBorderColor: ["#000000", "#000000"]
            }
          ],
          labels: ["Linux", "Windows"]
        };
        break;
      case "ide":
        data = {
          datasets: [
            {
              label: "IDE's",
              data: [90, 90, 40, 30],
              backgroundColor: ["#3FB47D", "#007ACC", "#8256AB", "#F7941D"],
              hoverBorderColor: ["#000000", "#000000", "#000000", "#000000"]
            }
          ],
          labels: [
            "Atom",
            "Visual Studio Code",
            "Intellij Idea",
            "Otros (Eclipse, Visual Studio, Sublime)"
          ]
        };
        break;

      case "layout":
        data = {
          datasets: [
            {
              label: "Lenguajes de maquetado",
              data: [90, 75],
              backgroundColor: ["#E44D26", "#1467AB"],
              hoverBorderColor: ["#000000", "#000000"]
            }
          ],
          labels: ["HTML5", "CSS3"]
        };
        break;

      case "frontend":
        data = {
          datasets: [
            {
              label: "Lenguajes front-end",
              data: [85, 80],
              backgroundColor: ["#F7DF1E", "#007ACC"],
              hoverBorderColor: ["#000000", "#000000"]
            }
          ],
          labels: ["Javascript", "Typescript"]
        };
        break;

      case "backend":
        data = {
          datasets: [
            {
              label: "Lenguajes back-end",
              data: [90, 40, 20],
              backgroundColor: ["#777BB3", "#CA4C31", "#953DAC"],
              hoverBorderColor: ["#000000", "#000000", "#000000"]
            }
          ],
          labels: ["PHP", "Java", "C#"]
        };
        break;

      case "frontFramework":
        data = {
          datasets: [
            {
              label: "Frameworks front-end",
              data: [80, 90],
              backgroundColor: ["#C3002F", "#3659AA"],
              hoverBorderColor: ["#000000", "#000000"]
            }
          ],
          labels: ["Angular 5", "JQuery"]
        };
        break;

      case "backFramework":
        data = {
          datasets: [
            {
              label: "Frameworks back-end",
              data: [50, 20],
              backgroundColor: ["#F4645F", "#FABF3F"],
              hoverBorderColor: ["#000000", "#000000"]
            }
          ],
          labels: ["Laravel", "CodeIgniter"]
        };
        break;

      case "layoutFramework":
        data = {
          datasets: [
            {
              label: "Frameworks maquetado",
              data: [90],
              backgroundColor: ["#5E4088"],
              hoverBorderColor: ["#000000"]
            }
          ],
          labels: ["Bootstrap 4"]
        };
        break;

      case "testing":
        data = {
          datasets: [
            {
              label: "Herramientas de testing",
              data: [50, 30],
              backgroundColor: ["#8A4182", "#B52F32"],
              hoverBorderColor: ["#000000", "#000000"]
            }
          ],
          labels: ["Jasmine + Karma", "Protractor"]
        };
        break;

      case "design":
        data = {
          datasets: [
            {
              label: "Herramientas de diseño",
              data: [70, 50, 20],
              backgroundColor: ["#2DBEEB", "#A975B7", "#F3E47D"],
              hoverBorderColor: ["#000000", "#000000", "#000000"]
            }
          ],
          labels: ["Photoshop", "Premiere", "Corel Draw"]
        };
        break;

      case "learning":
        data = {
          datasets: [
            {
              label: "Que estoy aprendiendo",
              data: [10, 5, 15],
              backgroundColor: ["#90C53F", "#00D8FF", "#CD6598"],
              hoverBorderColor: ["#000000", "#000000", "#000000"]
            }
          ],
          labels: ["Node JS", "React", "Sass"]
        };
        break;

      case "working":
        data = {
          datasets: [
            {
              label: "Herramientas de oficina y metodologías ágiles",
              data: [95, 70, 80],
              backgroundColor: ["#D91253", "#2B7A98", "#C2C2C2"],
              hoverBorderColor: ["#000000", "#000000", "#000000"]
            }
          ],
          labels: ["Slack", "Trello", "Scrum"]
        };
        break;
    }

    return data;
  }

  setVisibility(type: string) {
    switch (type) {
      case "database":
        this.database = true;
        this.repository = false;
        this.os = false;
        this.ide = false;
        this.layout = false;
        this.frontend = false;
        this.backend = false;
        this.frontFramework = false;
        this.backFramework = false;
        this.layoutFramework = false;
        this.testing = false;
        this.design = false;
        this.learning = false;
        this.working = false;
        break;
      case "repository":
        this.database = false;
        this.repository = true;
        this.os = false;
        this.ide = false;
        this.layout = false;
        this.frontend = false;
        this.backend = false;
        this.frontFramework = false;
        this.backFramework = false;
        this.layoutFramework = false;
        this.testing = false;
        this.design = false;
        this.learning = false;
        this.working = false;
        break;
      case "os":
        this.database = false;
        this.repository = false;
        this.os = true;
        this.ide = false;
        this.layout = false;
        this.frontend = false;
        this.backend = false;
        this.frontFramework = false;
        this.backFramework = false;
        this.layoutFramework = false;
        this.testing = false;
        this.design = false;
        this.learning = false;
        this.working = false;
        break;
      case "ide":
        this.database = false;
        this.repository = false;
        this.os = false;
        this.ide = true;
        this.layout = false;
        this.frontend = false;
        this.backend = false;
        this.frontFramework = false;
        this.backFramework = false;
        this.layoutFramework = false;
        this.testing = false;
        this.design = false;
        this.learning = false;
        this.working = false;
        break;
      case "layout":
        this.database = false;
        this.repository = false;
        this.os = false;
        this.ide = false;
        this.layout = true;
        this.frontend = false;
        this.backend = false;
        this.frontFramework = false;
        this.backFramework = false;
        this.layoutFramework = false;
        this.testing = false;
        this.design = false;
        this.learning = false;
        this.working = false;
        break;
      case "frontend":
        this.database = false;
        this.repository = false;
        this.os = false;
        this.ide = false;
        this.layout = false;
        this.frontend = true;
        this.backend = false;
        this.frontFramework = false;
        this.backFramework = false;
        this.layoutFramework = false;
        this.testing = false;
        this.design = false;
        this.learning = false;
        this.working = false;
        break;
      case "backend":
        this.database = false;
        this.repository = false;
        this.os = false;
        this.ide = false;
        this.layout = false;
        this.frontend = false;
        this.backend = true;
        this.frontFramework = false;
        this.backFramework = false;
        this.layoutFramework = false;
        this.testing = false;
        this.design = false;
        this.learning = false;
        this.working = false;
        break;
      case "frontFramework":
        this.database = false;
        this.repository = false;
        this.os = false;
        this.ide = false;
        this.layout = false;
        this.frontend = false;
        this.backend = false;
        this.frontFramework = true;
        this.backFramework = false;
        this.layoutFramework = false;
        this.testing = false;
        this.design = false;
        this.learning = false;
        this.working = false;
        break;
      case "backFramework":
        this.database = false;
        this.repository = false;
        this.os = false;
        this.ide = false;
        this.layout = false;
        this.frontend = false;
        this.backend = false;
        this.frontFramework = false;
        this.backFramework = true;
        this.layoutFramework = false;
        this.testing = false;
        this.design = false;
        this.learning = false;
        this.working = false;
        break;
      case "layoutFramework":
        this.database = false;
        this.repository = false;
        this.os = false;
        this.ide = false;
        this.layout = false;
        this.frontend = false;
        this.backend = false;
        this.frontFramework = false;
        this.backFramework = false;
        this.layoutFramework = true;
        this.testing = false;
        this.design = false;
        this.learning = false;
        this.working = false;
        break;
      case "testing":
        this.database = false;
        this.repository = false;
        this.os = false;
        this.ide = false;
        this.layout = false;
        this.frontend = false;
        this.backend = false;
        this.frontFramework = false;
        this.backFramework = false;
        this.layoutFramework = false;
        this.testing = true;
        this.design = false;
        this.learning = false;
        this.working = false;
        break;
      case "design":
        this.database = false;
        this.repository = false;
        this.os = false;
        this.ide = false;
        this.layout = false;
        this.frontend = false;
        this.backend = false;
        this.frontFramework = false;
        this.backFramework = false;
        this.layoutFramework = false;
        this.testing = false;
        this.design = true;
        this.learning = false;
        this.working = false;
        break;
      case "learning":
        this.database = false;
        this.repository = false;
        this.os = false;
        this.ide = false;
        this.layout = false;
        this.frontend = false;
        this.backend = false;
        this.frontFramework = false;
        this.backFramework = false;
        this.layoutFramework = false;
        this.testing = false;
        this.design = false;
        this.learning = true;
        this.working = false;
        break;
      case "working":
        this.database = false;
        this.repository = false;
        this.os = false;
        this.ide = false;
        this.layout = false;
        this.frontend = false;
        this.backend = false;
        this.frontFramework = false;
        this.backFramework = false;
        this.layoutFramework = false;
        this.testing = false;
        this.design = false;
        this.learning = false;
        this.working = true;
        break;
    }
  }
}
