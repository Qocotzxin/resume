import { Component, OnInit } from "@angular/core";
import * as $ from "jquery";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  programmer: boolean;
  personalData: boolean = true;
  education: boolean;
  experience: boolean;
  abilities: boolean;
  interests: boolean;
  loading: boolean;

  constructor() {}

  ngOnInit() {
    $(".cv-image").mouseover(() => {
      $(".cv-image").attr("src", "../assets/cv_personal_bside.png");
    });

    $(".cv-image").mouseout(() => {
      $(".cv-image").attr("src", "../assets/cv_personal.png");
    });
  }

  changePersonalData() {
    this.loading = true;
    this.personalData = false;
    this.programmer = false;
    this.education = false;
    this.experience = false;
    this.abilities = false;
    this.interests = false;
    
    setTimeout(() => {
      this.setFontWeight("not-selected");
      this.personalData = true;
      this.loading = false;

      if (this.personalData) {
        $(".information-titles .personalData").css("font-weight", "bolder");
      }
    }, 2000);
  }

  changeProgrammer() {
    this.loading = true;
    this.programmer = false;
    this.personalData = false;
    this.education = false;
    this.experience = false;
    this.abilities = false;
    this.interests = false;

    setTimeout(() => {
      this.setFontWeight("not-selected");
      this.programmer = true;
      this.loading = false;

      if (this.programmer) {
        this.setFontWeight("selected", ".programmer");
      }
    }, 2000);
  }

  changeEducation() {
    this.loading = true;
    this.education = false;
    this.personalData = false;
    this.programmer = false;
    this.experience = false;
    this.abilities = false;
    this.interests = false;

    setTimeout(() => {
      this.setFontWeight("not-selected");
      this.education = true;
      this.loading = false;

      if (this.education) {
        this.setFontWeight("selected", ".education");
      }
    }, 2000);
  }

  changeExperience() {
    this.loading = true;
    this.experience = false;
    this.personalData = false;
    this.programmer = false;
    this.education = false;
    this.abilities = false;
    this.interests = false;

    setTimeout(() => {
      this.setFontWeight("not-selected");
      this.experience = true;
      this.loading = false;

      if (this.experience) {
        this.setFontWeight("selected", ".experience");
      }
    }, 2000);
  }

  changeAbilities() {
    this.loading = true;
    this.abilities = false;
    this.personalData = false;
    this.programmer = false;
    this.education = false;
    this.experience = false;
    this.interests = false;

    setTimeout(() => {
      this.setFontWeight("not-selected");
      this.abilities = true;
      this.loading = false;

      if (this.abilities) {
        this.setFontWeight("selected", ".abilities");
      }
    }, 2000);
  }

  changeInterests() {
    this.loading = true;
    this.interests = false;
    this.personalData = false;
    this.programmer = false;
    this.education = false;
    this.experience = false;
    this.abilities = false;

    setTimeout(() => {
      this.setFontWeight("not-selected");
      this.interests = !this.interests;
      this.loading = false;

      if (this.interests) {
        this.setFontWeight("selected", ".interests");
      }
    }, 2000);
  }

  setFontWeight(state: string, element?: string) {
    if (state == "not-selected" && !element) {
      $(".vertical-header h3").css("font-weight", 200);
    } else if (state == "not-selected" && element) {
      $(".vertical-header " + element).css("font-weight", 200);
    } else if (state == "selected" && !element) {
      $(".vertical-header h3").css("font-weight", "bolder");
    } else if (state == "selected" && element) {
      $(".vertical-header " + element).css("font-weight", "bolder");
    }
  }
}
