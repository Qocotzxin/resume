import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FingerprintSpinnerModule } from "angular-epic-spinners";

import { AppComponent } from './app.component';
import { PersonalDataComponent } from './personal-data/personal-data.component';
import { ProgrammerComponent } from './programmer/programmer.component';
import { EducationComponent } from './education/education.component';
import { ExperienceComponent } from './experience/experience.component';
import { AbilitiesComponent } from './abilities/abilities.component';
import { InterestsComponent } from './interests/interests.component';


@NgModule({
  declarations: [
    AppComponent,
    PersonalDataComponent,
    ProgrammerComponent,
    EducationComponent,
    ExperienceComponent,
    AbilitiesComponent,
    InterestsComponent
  ],
  imports: [BrowserModule, FingerprintSpinnerModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
